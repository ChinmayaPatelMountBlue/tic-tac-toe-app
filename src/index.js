import React, { useState } from 'react';
import * as ReactDOM from 'react-dom/client';
import './index.css';

function Square(props) {

      return (
        <button 
          className="square"
          onClick={props.onClick}
        >
          {props.value}
        </button>
      );
}
  
function Board(props) {

  function renderSquare(i) {
      return <Square
                value={props.squares[i]}
                onClick={() => props.handleClick(i)}
             />;
    }
  
    return (
        <div>
          <div className="board-row">
            {renderSquare(0)}
            {renderSquare(1)}
            {renderSquare(2)}
          </div>
          <div className="board-row">
            {renderSquare(3)}
            {renderSquare(4)}
            {renderSquare(5)}
          </div>
          <div className="board-row">
            {renderSquare(6)}
            {renderSquare(7)}
            {renderSquare(8)}
          </div>
        </div>
    );
}
  
function Game() {

  const [xIsNext, setXIsNext] = useState(true);

  const [historyLog, setHistoryLog] = useState(
    [
      {
        squares: Array(9).fill(null)
      }
    ]
  );

  const [stepNumber, setStepNumber] = useState(0);

  const history = historyLog.slice(0, stepNumber + 1);

  const current = history[history.length-1];

  function handleClick(i){

    const currentSquares = current.squares.slice();
    if(calculateWinner(currentSquares) || currentSquares[i]){
      return;
    }
    currentSquares[i] = xIsNext ? 'X' : 'O' ;
    setXIsNext(!xIsNext);
    history.push({squares: currentSquares});
    setHistoryLog(history);
    setStepNumber(historyLog.length);

  }

  function jumpTo(step) {

    setStepNumber(step);

    setXIsNext(((step % 2) === 0) ? true : false );

  }

  const calculateWinner = (squares) => {

    const lines = [
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,3,6],
      [1,4,7],
      [2,5,8],
      [0,4,8],
      [2,4,6]
    ];

    for(let index = 0; index < lines.length; index++){

      const [a,b,c] = lines[index];

      if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]){

        return squares[a];

      }

    }

    return null;

  }

  let winner = calculateWinner(history[history.length-1]["squares"]);

  const moves = historyLog.map((step,move) => {

    const desc = move ? 'Go to move #' + move : 'Go to Game Start' ;

    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{desc}</button>
      </li>
    );

  })

  let status;

  if(winner){
    status = 'Winner: ' + winner;
  }
  else if(historyLog.length === 10){
    status = 'Draw!';
  }
  else{
    status = 'Next Player: ' + (xIsNext ? 'X' : 'O') ;
  }

    return (
        <div className="game">
          <div className="game-board">
            <Board
              squares = {current.squares}
              handleClick = {(i) => handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
    );
}

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <React.StrictMode>
        <Game />
    </React.StrictMode>
); 